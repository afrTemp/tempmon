﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace temperatureMonitor
{
    internal class MeasurementData
    {
        private static List<MeasurementData> measurementDataToSendBuffer = new List<MeasurementData>(); //static list of networkhandlers to store up to the 10 last measurements
        public static List<Tuple<double, DateTime>> dateTempValues = new List<Tuple<double, DateTime>>(); //stores the temp vals and the time

        public Timestamp time = new Timestamp();
        public double min { get; set; }
        public double max { get; set; }
        public double avg { get; set; }

        public MeasurementData(DateTime startDate, DateTime endDate, double minVal, double maxVal, double avgVal)   //TODO: perhaps create parent class to handle the initial timestamp+temp and then append the min/max/avg values to this
        {
            time.start = startDate;
            time.end = endDate;
            min = minVal;
            max = maxVal;
            avg = avgVal;
        }

        internal class Timestamp    //ugly, but the json serializaton didnt wanna cooperate without a nested class
        {
            public DateTime start { get; set; }
            public DateTime end { get; set; }
        }

        public static void AddMeasurementData(MeasurementData data)
        {
            if (measurementDataToSendBuffer.Count == 10)
            {
                measurementDataToSendBuffer.RemoveAt(0);  //remove the oldest object if the list is 
            }
            else
            {
                measurementDataToSendBuffer.Add(data);
            }
        }
        public static List<MeasurementData> GetMeasurementData()
        {
            return measurementDataToSendBuffer;
        }
    }
}
