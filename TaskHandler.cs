﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace temperatureMonitor
{
    internal class TaskHandler
    {
        private readonly int _readDelay = 100; //100ms delay
        private readonly int _calcDelay = 120000; //2 minute delay
        private StreamReader? _reader = new StreamReader("temperature.txt");

        //reads adc value from file and returns the temperature
        public async Task GetTemperature()
        {
            while (true)
            {
                if (_reader == null)
                {
                    throw new InvalidOperationException("StreamReader not initialized.");
                }

                try
                {
                    string? line = await _reader.ReadLineAsync();

                    if (_reader.EndOfStream)
                    {
                        _reader.BaseStream.Position = 0; //nasty but assuming it's an actual ADC datastream and not a text file you wouldnt run out of (potentially) valid values
                    }

                    double adcValue = Convert.ToDouble(line);
                    double temperature = Calculations.ConvertToTemperature(adcValue);

                    MeasurementData.dateTempValues.Add(new Tuple<double, DateTime>(temperature, DateTime.UtcNow)); //rolling buffer of 2-minute values

                    await Task.Delay(_readDelay); //delay task in order to read only once every 100ms

                    Console.WriteLine(temperature);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }
            }
        }

        //calculates the max, min and avg temperature values every 2 minutes
        public async Task SendSensorData()
        {
            while (true)
            {
                try
                {
                    //TODO: add production flag to always use _calcDelay instead of random times for testing
                    await Task.Delay(_calcDelay); //wait for 2 minutes before calculating the values and sending them to the endpoint

                    var (max, min, avg) = Calculations.CalculateValues(MeasurementData.dateTempValues.Select(x => x.Item1).ToList());
                    
                    Console.WriteLine("===============calculated===============");
                    Console.WriteLine($"Max: {max}, Min: {min}, Avg: {avg}");
                    Console.WriteLine("===============calculated===============");

                    await NetworkHandler.SendDataToEndpointAsync(new MeasurementData
                        (   MeasurementData.dateTempValues[0].Item2,
                            MeasurementData.dateTempValues[^1].Item2,
                            min, 
                            max, 
                            avg)); //send the data to the endpoint

                    MeasurementData.dateTempValues.Clear();    //clear the buffer for the next 2 minutes
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }
            }
        }

        //closes the StreamReader
        public void Close()
        {
            _reader?.Close();
            _reader = null;
        }
    }
}

