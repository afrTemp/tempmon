﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace temperatureMonitor
{
    internal class Calculations
    {
        public static readonly int adcResolution = 4096; //12 bit ADC

        //calculates temperature from ADC value in the range of -50 to 50 degrees C
        public static double ConvertToTemperature(double adcValue)
        {
            return Math.Round((adcValue / adcResolution) * 100 - 50, 2);
        }

        //calculates the max, min and avg temp from the list of temperature values
        public static (double max, double min, double average) CalculateValues(List<double> values)
        {
            double max = values.Max();
            double min = values.Min();
            double average = values.Average();

            return (max, min, average);
        }
    }
}
