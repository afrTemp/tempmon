﻿using temperatureMonitor;

internal class Program
{
    static void Main(string[] args)
    {
        TaskHandler reader = new TaskHandler();


        // Run both tasks concurrently
        var fileTask = Task.Run(() => reader.GetTemperature());
        var statsTask = Task.Run(() => reader.SendSensorData());

        Console.ReadLine();
        reader.Close();
        fileTask.Dispose();
        statsTask.Dispose();
    }
}