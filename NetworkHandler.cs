﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace temperatureMonitor
{
    internal class NetworkHandler
    {
        //TODO: implement generalized error handling of REST api calls
        private void ErrorHandler()
        {

        }

        public static async Task SendDataToEndpointAsync(MeasurementData mData)
        {
            string json = JsonConvert.SerializeObject(mData);


            using (var client = new HttpClient())
            {
                var apiUrl = "http://localhost:5000/api/temperature"; //api 
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(apiUrl, content);

                if (!response.IsSuccessStatusCode)
                {
                    MeasurementData.AddMeasurementData(mData);

                    //convert up to the last 10 networkHandlerBuf objects into a JSON array
                    json = JsonConvert.SerializeObject(MeasurementData.GetMeasurementData());

                    apiUrl = "http://localhost:5000/api/temperature/missing"; //api endpoint
                    content = new StringContent(json, Encoding.UTF8, "application/json");
                    response = await client.PostAsync(apiUrl, content); //currently assuming that this endpoint will not fail and/or data loss is acceptable
                }
            }
        }
    }
}